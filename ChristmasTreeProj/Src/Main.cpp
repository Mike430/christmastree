#include <stdio.h>

#include <ChristmasTree.h>

int main()
{
    printf("Please enter a number:");
    int count = 0;
    scanf("%d", &count);
    getchar();// sweep up the return character

    printf("%s", ChristmasTree::CreateTree(count).c_str());

    printf("\n\nPress ENTER to continue...");
    getchar();
}