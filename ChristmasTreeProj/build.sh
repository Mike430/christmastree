printf "\nChristmas Tree Proj - build script started\n"

#https://stackoverflow.com/questions/24112727/relative-paths-based-on-file-location-instead-of-current-working-directory/24113238
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd "$parent_path"

# -static-libstdc++ - fixes missing DLL errors on windows - https://stackoverflow.com/questions/6404636/libstdc-6-dll-not-found
clang++ -std=c++11 -Wall -static-libstdc++ -o Executable.exe -I ../ChristmasLibProj/Include/ Src/Main.cpp ../ChristmasLibProj/BuildOutput/ChristmasTree.a

if [[ ! -f Executable.exe ]] ; then
    printf "\nThe compile failed - aborting!\n"
    exit 1
fi

mkdir -p BuildOutput
mv Executable.exe BuildOutput/

printf "\nChristmas Tree Proj - build script finnished\n"