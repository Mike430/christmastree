printf "\nChristmas Lib Proj - build script started\n"

#https://stackoverflow.com/questions/24112727/relative-paths-based-on-file-location-instead-of-current-working-directory/24113238
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd "$parent_path"

# -g = generate debug information
# -c = generate a .o object file - doesn't look for a main or WinMain function
# -v = verbose
# -o = output file name
# -I = include path
clang++ -std=c++11 -Wall -c -o ChristmasTree.o -I Include/ Src/ChristmasTree.cpp

if [[ ! -f ChristmasTree.o ]] ; then
    printf "\nThe compile failed - aborting!\n"
    exit 1
fi

llvm-ar rc ChristmasTree.a ChristmasTree.o

mkdir -p BuildOutput/
mv ChristmasTree.* BuildOutput/

printf "\nChristmas Lib Proj - build script finnished\n"