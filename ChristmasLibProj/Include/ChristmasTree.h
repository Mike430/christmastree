#pragma once

#include <string>

namespace ChristmasTree
{
    bool CanPrintTree(int size);
    std::string CreateTree(int size);
}