#include "ChristmasTree.h"

int maxTreeSize = 20;

bool ChristmasTree::CanPrintTree(int size)
{
    return size < maxTreeSize;
}

std::string ChristmasTree::CreateTree(int size)
{
    std::string output = "";

    if(!CanPrintTree(size))
    {
        output = "Your tree is too big";
    }
    else
    {
        output += "\n"; // Make this one of your test cases - does the string start with a new line?

        // Draw the star
        //======================================
        for(int i = 0; i < size; ++i)
        {
            output += " ";
        }

        output += "*";

        // Draw the body
        //======================================
        for(int layer = 0; layer < size; ++layer)
        {
            output += "\n";

            for(int indent = 0; indent < (size - layer)-1; ++indent)
            {
                output += " ";
            }

            output += "/o";

            for(int balls = 0; balls < layer; ++balls)
            {
                output += "_o";
            }

            output += "\\";
        }

        // Draw the tree stump
        //======================================
        output += "\n";

        for(int i = 0; i < size-1; ++i)
        {
            output += " ";
        }
        output += "|_|";
    }

    return output;
}